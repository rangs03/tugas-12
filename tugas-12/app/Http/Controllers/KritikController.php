<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request, $id){
        $request->validate([
            'content' => 'required|min:5'
        ]);

        $user_id = Auth::id();
        
        $kritik = new Kritik;

        $kritik->content = $request['content'];
        $kritik->film_id = $id;
        $kritik->user_id = $user_id;
        $kritik->point = 1;
        $kritik->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan Kritik');

        return redirect('/film/'. $id);
    }
}
