<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Film;
use File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = FIlm::all();

        return view('film.index', ['film' => $film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.createFilm', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // validasi data
         $request->validate([
            'judul' => 'required|min:5',
            'ringkasan' => 'required|min:50',
            'tahun' => 'required|numeric|',
            'poster' => 'required|mimes:png,jpeg,jpg',
            'genre_id' => 'required'
        ]);
        // ubah nama file menjadi unique
        $newNamePoster = time(). '.' .$request->poster->extension();

        // pindahkan file ke folder public di dalam folder image
        $request->poster->move(public_path('image'), $newNamePoster);

        // masukan data request ke db table film dengan orm
        $film = new Film;

        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->poster = $newNamePoster;
        $film->genre_id = $request['genre_id'];

        $film->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan Film');
        // lempar ke halaman /film
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        return view('film.detail', ['film' => $film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi data
        $request->validate([
            'judul' => 'required|min:5',
            'ringkasan' => 'required|min:50',
            'tahun' => 'required|numeric|',
            'poster' => 'mimes:png,jpeg,jpg',
            'genre_id' => 'required'
        ]);

        // fungsi update
        $film = Film::find($id);

        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->genre_id = $request['genre_id'];

        if ($request->has('poster')){
            $path = 'image/';
            File::delete($path. $film->poster);

            // ubah nama file menjadi unique
            $newNamePoster = time(). '.' .$request->poster->extension();

            // pindahkan file ke folder public di dalam folder image
            $request->poster->move(public_path('image'), $newNamePoster);

            $film->poster = $newNamePoster;
        }

        $film->save();

        Alert::success('Berhasil', 'Berhasil Mengedit Film');

        // lempar ke halaman /film
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $path = 'image/';
        File::delete($path. $film->poster);

        $film->delete();

        Alert::success('Berhasil', 'Berhasil Menghapus Film');

        // lempar ke halaman /film
        return redirect('/film');
    }
}
