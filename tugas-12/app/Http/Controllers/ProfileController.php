<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index(){
        $user_id = Auth::id();

        $profile = Profile::where('user_id', $user_id)->first();
        return view('profile.index', ['profile' => $profile]);
    }

    public function update(Request $request, $id){
         // validasi data
         $request->validate([
            'umur' => 'required|numeric|min:1',
            'bio' => 'required|min:10',
            'alamat' => 'required|min:10'
        ]);

        // update data table
        $profile = Profile::find($id);

        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];

        $profile->save();

        // lempar ke halaman /profile
        return redirect('/profile');
    }
}
