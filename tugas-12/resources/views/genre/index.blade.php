@extends('layout.master')
@section('title')
Halaman Genre
@endsection

@section('content')

@push('scripts')
</script>
<script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
$(function () {
    $("#example1").DataTable();
});
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>
@endpush

@auth
<a href="/genre/create" class="btn btn-primary btn-sm my-2">Tambah</a>
@endauth

<table class="table table-hover table-striped" id="example1">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        @auth
        <th scope="col">Action</th>
        @endauth
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                @auth
                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
                @endauth
            </tr>
        @empty
            <tr>
                <td colspan="4">Data Genre Kosong</td>
            </tr>
        @endforelse
      
    </tbody>
</table>

@endsection