@extends('layout.master')
@section('title')
Halaman Edit Genre
@endsection

@section('content')

<form method="POST" action="/genre/{{$genre->id}}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputnama">Nama</label>
        <input type="text" name="nama" class="form-control" value="{{$genre->nama}}">
    <div class="form-group">
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection