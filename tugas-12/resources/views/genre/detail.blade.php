@extends('layout.master')
@section('title')
Halaman Film
@endsection

@section('content')

<h1 class="my-2">{{$genre->nama}}</h1>

<div class="row">
    @forelse ($genre->film as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" height="300px" alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->judul}} ({{$item->tahun}})</h2>
              <p class="card-text">{{ Str::limit($item->ringkasan, 30)}}</p>
              <a href="/film/{{$item->id}}" class="btn btn-block btn-info my-2 ">Detail</a>
            </div>
        </div>
    </div>
    @empty
        <small>Belum Ada Daftar Film</small>
    @endforelse
</div>

@endsection