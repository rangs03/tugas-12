@extends('layout.master')
@section('title')
Halaman Tambah Genre
@endsection

@section('content')

<form method="POST" action="/genre">
    @csrf
    <div class="form-group">
        <label for="exampleInputnama">Nama</label>
        <input type="text" name="nama" class="form-control">
    <div class="form-group">
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection