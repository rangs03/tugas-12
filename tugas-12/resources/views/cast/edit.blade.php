@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection

@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputnama">Nama</label>
        <input type="text" name="nama" class="form-control" value="{{$cast->nama}}">
    <div class="form-group">
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
      <label for="exampleInputUmur">Umur</label>
      <input type="number" name="umur" class="form-control" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputBio">Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection