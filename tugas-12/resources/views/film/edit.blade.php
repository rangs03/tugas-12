@extends('layout.master')
@section('title')
Halaman Edit Film
@endsection

@section('content')

<form method="POST" enctype="multipart/form-data" action="/film/{{$film->id}}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputjudul">Judul</label>
        <input type="text" name="judul" class="form-control" value="{{$film->judul}}">
    <div>
    @error('judul')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputRingkasan">Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputTahun">Tahun</label>
        <input type="number" name="tahun" class="form-control" value="{{$film->tahun}}">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPoster">Poster</label>
        <input type="file" name="poster" class="form-control">
    </div>
    <div class="form-group">
        <label for="exampleInputGenre">Genre</label>
        <select class="form-control" name="genre_id" id="genre_id">
            <option value="">--Pilih Genre--</option>
            @forelse ($genre as $item)
                @if ($item->id === $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>    
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>                     
                @endif
            @empty
                <option value="">Belum ada data kategori</option>
            @endforelse
        </select>
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection