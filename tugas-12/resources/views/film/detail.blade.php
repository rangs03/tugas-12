@extends('layout.master')
@section('title')
Halaman Detail Film
@endsection

@section('content')

@push('scripts')
<script src="https://cdn.tiny.cloud/1/5echw8dq75sc8k4bbktajagke2lhmvn8e5v1m0ocgf4olq7v/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mergetags_list: [
      { value: 'First.Name', title: 'First Name' },
      { value: 'Email', title: 'Email' },
    ]
  });
</script>
@endpush


<div class="card">
  <img class="card-img-top" src="{{asset('/image/'.$film->poster)}}" alt="Card image cap">
  <div class="card-body">
    <h2>{{$film->judul}}</h2>
    <h2>Tayang pada tahun : {{$film->tahun}}</h2>
    <p class="card-text">{{$film->ringkasan}}</p>
  </div>
  
  <hr>
  <h4>List Kritik</h4>
  <hr>

  @forelse ($film->kritik as $item)
  <div class="card my-3">
    <div class="card-header">
      {{$item->user->name}}
    </div>
    <div class="card-body">
      <p class="card-text">{!!$item->content!!}</p>
    </div>
  </div>
  @empty
      <h6>Belum ada kritik</h6>
  @endforelse

  @auth
  <form action="/kritik/{{$film->id}}" method="POST">
    @csrf
    <textarea name="content" class="form-control" placeholder="Isi kritik" id="" cols="30" rows="10"></textarea>
    
    @error('content')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <input type="submit" class="btn btn-primary btn-sm my-2" value="Kirim Kritik">
  </form>
  @endauth

</div>
  
<a href="/film" class="btn btn-block btn-success my-2 ">Kembali</a>
  
@endsection