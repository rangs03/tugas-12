<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KritikController;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Route::get('/data-tables', function () {
    return view('pages.datatable');
});

Route::get('/table', function () {
    return view('pages.table');
});

Route::group(['middleware' => ['auth']], function () {
    //
    // CRUD

    // create table
    // rute untuk mengarah ke form tambah cast
    Route::get('/cast/create', [CastController::class, 'create']);
    // rute untuk menyimpan data inputan ke db table cast
    Route::post('/cast', [CastController::class, 'store']);

    // update table
    // mengarah ke form edit cast dengan membawa data berdasarkan id
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
    // update kategori berdasarkan id
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);

    // delete table
    // rute untuk menghapus data berdasarkan id
    Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
});
// read table
// menampilkan semua data yang ada di table cast
Route::get('/cast', [CastController::class, 'index']);
// menampilkan detail data cast tertentu berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::resource('/film', FilmController::class);
Route::resource('/genre', GenreController::class);

Route::get('/profile', [ProfileController::class, 'index']);
Route::put('/profile/{id}', [ProfileController::class, 'update']);

// kritik
Route::post('/kritik/{film_id}', [KritikController::class, 'store']);

Auth::routes();
